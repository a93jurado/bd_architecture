from google.cloud import storage

from app.config import BUCKET_NAME, CREDENTIALS

class Storage:
  def __init__(self, bucket_name):
    self.client = storage.Client.from_service_account_json(f'./{CREDENTIALS}')
    self.bucket = self.client.get_bucket(bucket_name)

  def upload(self, name, path_to_file):
    blob = self.bucket.blob(name)
    blob.upload_from_filename(path_to_file)

    return blob.public_url


cloud_storage = Storage(bucket_name=BUCKET_NAME)
