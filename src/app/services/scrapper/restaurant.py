import re
import uuid

from app.services.scrapper.opinion import Opinion

class Restaurant:
  def __init__(self, response):
    self.response = response

  def parse_address(self):
    address = self.response.css('.address')
    street_address = address.css('.street-address ::text').extract_first() or ''
    extended_address = address.css(
        '.extended-address ::text').extract_first() or ''
        
    return f'{street_address} {extended_address}'

  def parse_phone(self):
    return self.response.css('.phone > a > .detail ::text').extract_first() or ''

  def parse_name(self):
    return self.response.css('.restaurantName > h1 ::text').extract_first() or ''

  def parse_cuisines(self):
    cuisines_source = self.response.css('.header_links > a ::text').extract()
    filtered = filter(lambda cuisine: '€' not in cuisine, cuisines_source)

    return '|'.join(set(filtered))

  def parse_review(self):
    review_count_source = self.response.css(
        '.ratingContainer > a > .reviewCount ::text').extract_first() or ''
    search = re.search(r'[\d\.\d]+', review_count_source)

    return search.group() if search is not None else ''

  def parse_restaurant(self):
    opinions = Opinion(response=self.response)

    return {
        'id': uuid.uuid4().hex,
        'name': self.parse_name(),
        'review_count': self.parse_review(),
        'cuisines': self.parse_cuisines(),
        'address': self.parse_address(),
        'phone': self.parse_phone(),
        'opinions': opinions.parse_opinions()
    }


