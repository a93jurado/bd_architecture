import scrapy
import csv

from app.config import HOST, DETAIL
from app.services.scrapper.restaurant import Restaurant
from app.services.gcloud import Storage
from app.config import OUTPUT_FILENAME

class RestaurantsSpider(scrapy.Spider):
  name = "restaurants"
  start_urls = [f'{HOST}/{DETAIL}']

  def __init__(self):
    self.csvfile = open(f'output/{OUTPUT_FILENAME}', 'w')
    field_names = ['id', 'name', 'review_count',
                  'cuisines', 'address', 'phone', 'opinions']
    self.writer = csv.DictWriter(self.csvfile, fieldnames=field_names)

  def parse(self, response):
    uri = response.css('.nav.next.rndBtn.ui_button.primary.taLnk::attr(href)').extract_first()
    next_url = f'{HOST}/{uri}'
    yield response.follow(next_url, self.parse)

    for summary in response.css('.listing.rebrand'):
      restaurant_url = summary.css('.title > a::attr(href)').extract_first()
      url = f'{HOST}{restaurant_url}'
      yield response.follow(url, self.parse_product)

  def parse_product(self, response):
    parsed_restaurant = Restaurant(response=response).parse_restaurant()
    self.writer.writerow(parsed_restaurant)

  def close(self):
    self.csvfile.close()