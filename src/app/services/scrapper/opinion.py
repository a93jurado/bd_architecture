class Opinion():
  def __init__(self, response):
    self.response = response

  @staticmethod
  def parse_opinion(opinion):
    quote = opinion.css('.quote > a > span ::text').extract_first()
    abstract = opinion.css('.entry > p ::text').extract_first()

    return {'quote': quote, 'abstract': abstract}

  def parse_opinions(self):
    opinions = self.response.css('.review-container')
    
    return list(map(self.parse_opinion, opinions))