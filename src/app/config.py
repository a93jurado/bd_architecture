import os

HOST = os.getenv('HOST')
DETAIL = os.getenv('DETAIL')
CREDENTIALS = os.getenv('CREDENTIALS')
BUCKET_NAME = os.getenv('BUCKET_NAME')
OUTPUT_FILENAME = os.getenv('OUTPUT_FILENAME', 'output.csv')
 
