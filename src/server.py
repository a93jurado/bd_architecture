from scrapy.crawler import CrawlerProcess

from app.config import HOST, DETAIL, OUTPUT_FILENAME
from app.services.scrapper import RestaurantsSpider
from app.services.gcloud import cloud_storage

if __name__ == '__main__':
  print(f'Crawling {HOST}/{DETAIL}')

  process = CrawlerProcess()
  process.crawl(RestaurantsSpider)
  process.start()

  link = cloud_storage.upload(name=OUTPUT_FILENAME,
                        path_to_file=f'output/{OUTPUT_FILENAME}')
  print(f'File uploaded to {link}')
