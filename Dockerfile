FROM python:3.6-alpine

ENV HOME /crawler

COPY requirements.txt *.json ${HOME}/

WORKDIR ${HOME}
RUN apk update && apk add gcc python3-dev musl-dev libxml2-dev libxslt-dev openssl-dev libffi-dev  
RUN pip install -r requirements.txt

COPY entrypoint.sh /
RUN chmod 755 /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]

