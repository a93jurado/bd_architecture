#!/usr/bin/env bash

set -a
source services.envar

echo "Launching Scrapper..."
docker-compose -f docker-compose.yml up -d --build