# BDA
## Esquema de la arquitectura
![enter image description here](https://i.imgur.com/xFZDPGl.png)


## Creación de la arquitectura en GCloud
Para facilitar la ejecución del scrapper encargado de obtener el conjunto de datos, se ha creado un servicio que corre "contenizado" en docker, usando la imagen de Python-alpine.

Antes de la creación del clúster Dataproc, se creó un segmento en Cloud Storage. Esto facilitará la ingesta de datos para las tareas que se ejecutan en Hadoop, así como un almacenamiento persistente para el clúster, de tal forma que no depende de las instancias VM.

El clúster se ha realizado dentro de la misma región del segmento. En este caso europe-west1.

## Ejecución
Puesto que el servicio arroja sus resultados a Google Storage hay que proveerle del fichero, de autenticación, **.json** adecuado. Almacenado en el directorio de la aplicación, solo es necesario indicar, mediante variables de entorno su  ubicación. Ejemplo de variables de entorno:

```
export HOST=https://www.tripadvisor.es
export DETAIL=Restaurants-g187497-Barcelona_Catalonia.html
export CREDENTIALS=credentials.json
export BUCKET_NAME=bd_storage1
export OUTPUT_FILENAME=output.csv
```

Mediante HOST y DETAIL el Crawler crea las rutas necesarias para realizar la recopilación de los datos. Es parametrizable, ya que que en este caso se obtiene la información de los restaurantes de Barcelona, pero se puede extender a más provincias y localidades.

CREDENTIALS, el path por defecto a partir del directorio de ejecución.
BUCKET_NAME, indica el segmento de Google storage donde se almacenará el resultado bajo el nombre de OUTPUT_FILENAME.

Una vez configuradas las variables de entorno editando el fichero **services.envar**,  basta con:

```
bash run-docker.sh
```

## Word count

Una vez finalizado la ejecución del servicio, se encontrará en el segmento de Google Storage, un fichero .csv con los datos obtenidos.  Cada fila tiene la siguiente estructura:
```
id, name, review_count, cuisine1|cusine2|....., address, phone, opinions
```

Para realizar la cuenta de apariciones de cada palabra, solo es necesario ejecutar una tarea map-reducer con los argumentos adecuados. 

![enter image description here](https://i.imgur.com/GoNwHeu.jpg)

El input se encuentra almacenado en el segmento que una vez efectuada la tarea almacenará también el resultado.

Podemos ver los ficheros generados:
```
hdfs dfs -ls gs://bd_storage1/wordcount

-rwx------   3 fco_jurado93 fco_jurado93          0 2019-01-26 14:40 gs://bd_storage1/wordcount/_SUCCESS
-rwx------   3 fco_jurado93 fco_jurado93     614439 2019-01-26 14:40 gs://bd_storage1/wordcount/part-r-00000
-rwx------   3 fco_jurado93 fco_jurado93     614134 2019-01-26 14:40 gs://bd_storage1/wordcount/part-r-00001
-rwx------   3 fco_jurado93 fco_jurado93     606118 2019-01-26 14:40 gs://bd_storage1/wordcount/part-r-00002
-rwx------   3 fco_jurado93 fco_jurado93     607808 2019-01-26 14:40 gs://bd_storage1/wordcount/part-r-00003
-rwx------   3 fco_jurado93 fco_jurado93     613064 2019-01-26 14:40 gs://bd_storage1/wordcount/part-r-00004
```
Ejemplo obtenido con hdfs dfs -cat gs://bd_storage1/wordcount/part-r-00000
```
representa      1
representación  3
representado    1
representante   1
representativo  3
reprimir        1
reprochable     1
reprochar       2
```

## Hive

En primer lugar, será necesaria la creación de la tabla, para ello he usado el cliente **beeline**, desde el nodo master del cluster.
````
beeline -u "jdbc:hive2://localhost:10000"
````

````
create external table restaurants (
	id string,
	name string,
	review_count integer,
	cuisines array<string>,
	address string,
	phone string
)
row format delimited
fields terminated by ','
collection items terminated by '|'
location 'gs://bd_storage1/restaurants';
````
Una vez creada la tabla es necesario cargarle los datos.
````
LOAD DATA INPATH 'gs://bd_storage1/output.csv' INTO TABLE restaurants;
````
Se omite la palabra LOCAL, de tal forma que toma los ficheros desde HDFS y por lo tanto se puede acceder a los archivos del segmento.

Ya está la información cargada en la tabla y se puede realizar query sobre los datos. Por ejemplo:
````

SELECT name, review_count, cuisines FROM restaurants LIMIT 5;

+------------------+---------------+---------------------------------------------+
|       name       | review_count  |                  cuisines                   |
+------------------+---------------+---------------------------------------------+
| Accés            | 2             | ["Fusión","Mediterránea","Europea"]         |
| Bodega Biarritz  | 1             | ["Mediterránea","Española"]                 |
| Blavis           | 771           | ["Mediterránea","Española","Europea"]       |
| Billy Brunch     | 155           | ["Americana","Café","Europea"]              |
| Abanik Bar       | 137           | ["Mediterránea","Internacional","Europea"]  |
+------------------+---------------+---------------------------------------------+
````
Incluso realizar búsqueda por el tipo de cocina:
````
SELECT name, review_count, cuisines from restaurants WHERE array_contains(cuisines, 'Americana') LIMIT 5;

+----------------------------+---------------+----------------------------------------------------+
|            name            | review_count  |                      cuisines                      |
+----------------------------+---------------+----------------------------------------------------+
| Billy Brunch               | 155           | ["Americana","Café","Europea"]                     |
| Foc i Oli                  | 917           | ["Americana","Opciones vegetarianas"]              |
| Milk Bar & Bistro          | 2             | ["Americana","Internacional"]                      |
| Can Dendê                  | 860           | ["Americana","Café"]                               |
| National burger Barcelona  | 874           | ["Americana","Barbacoa","Asador"]                  |
+----------------------------+---------------+----------------------------------------------------+